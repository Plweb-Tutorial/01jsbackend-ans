Clone from gitlab

git clone https://gitlab.com/Plweb-Tutorial/01jsbackend-ans.git

Start server

npm start or nodemon start

##如何預備Project IDE 的教材

1、將Ans目錄及Part目錄分置於兩個目錄中  
2、改寫Ans目錄及Part目錄的submit.js   
3、請參考本目錄中的submit.js 將check()裡的 sentdata()更改為json()  
4、json() 與sentdata()程式碼如下  

    function json()
    {
    var fs = require('fs');
    
    fs.readFile('../../../bat/key.txt', function (err, data) {
        if (err) 
        {
            console.log('err'+err);
        }
        else{
        sentdata(data);
        }
    });
    }

    function sentdata(key){
    var state = true;
    let keys =String(key);
    console.log(result);

    for(var i=0; i<result.length; i++){
        if(result[i]=="Fail"){
        state = false;
        break;
        }
    }

    const postData = querystring.stringify({
        'id': path.parse(path.join(__dirname, "..")).name,
        'exercise': path.parse(__dirname).name,
        'key':keys,
        'state': state
    });

    const options = {
        hostname: '140.125.90.232',
        port: 3000,
        path: '/ProjectIDE/compiler',
        method: 'POST',
        headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': Buffer.byteLength(postData)
        }
    };
    
    const req = http.request(options, 
    function (res){
        //console.log(`STATUS: ${res.statusCode}`);
        //console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
        res.setEncoding('utf8');

        res.on('data', 
        function (chunk) {
        //console.log(`BODY: ${chunk}`);
        });
        
        res.on('end', 
        function(){
        console.log('提交成功');
        });
    });
    
    req.on('error', 
    function(e) {
        console.error(`提交失敗: ${e.message}`);
    });

    req.write(postData);
    console.log(querystring.parse(postData));
    req.end();
    }
5、點選gitlab.com 中的PLWeb-Tutorial建立一個新的Project，例如 06Jsbackend-ans 點選Visibility Level為 Public，然後Create project  
6、下已下指令  

    Git global setup 
    git config --global user.name "Sho-Huan Tung" 
    git config --global user.email "tungsh@yuntech.edu.tw" 
    
    cd 06Jsbackend-ans
    git init
    git remote add origin https://gitlab.com/Plweb-Tutorial/06jsbackend-ans.git
    git add .
    git commit -m "Initial commit"
    git push -u origin master
    
7、依此類推 例如 06Jsbackend-part 點選Visibility Level為 Public，然後Create project  